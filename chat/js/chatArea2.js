$(document).ready(function(){   
		 
	var image;
	 $("#container7").hide();
	    $("#container8, #container9").hide();
	    $("#contact .left-side li").click(function(){
	    	$("#container7").show();
	        var name = $("div>div>h4", this).text();
	        image = $("div>div>img", this).attr("src");
	        $("#container7 .navbar-brand").text(name);
	        
	        $("#container7 i").unbind('click').click(function(){
		 		var cls = $(this).hasClass("fa fa-angle-right");
		       if(cls == true) 	{
		 		   $(this).removeClass("fa fa-angle-right").addClass("fa fa-angle-down");
		           $("#container8").show();
		 	    }
		         else{
		        	 $(this).removeClass("fa fa-angle-down").addClass("fa fa-angle-right");
		             $("#container8").hide();
		            } 
		       });
	    });
	    
	    $("#groups .left-side li").click(function(){
	    	$("#container7").show();
	        var name = $("div>div>h4", this).text();
	        image = $("div>div>img", this).attr("src");
	        $("#container7 .navbar-brand").text(name);
	        
	        $("#container7 i").unbind('click').click(function(){
		 		var cls = $(this).hasClass("fa fa-angle-right");
		       if(cls == true) 	{
		 		   $(this).removeClass("fa fa-angle-right").addClass("fa fa-angle-down");
		           $("#container9").show();
		 	    }
		         else{
		        	 $(this).removeClass("fa fa-angle-down").addClass("fa fa-angle-right");
		             $("#container9").hide();
		            } 
		       });
	    });
	    
	    
	    $("#msgInput input").keypress(function(e){
	        if(e.which == 13) {
	            $("#send").click();
	        }
	    });
	    	 	
	 	
       $(".glyphicon-facetime-video").click(function(){
    	   $("#myModal1").modal();
    	   var name = $("#container7 .navbar-brand").text();
    	   $("#myModal1 label").text(name);
    	   $("#img1").attr("src",image);
       });
       
       $(".glyphicon-earphone").click(function(){
    	   $("#myModal2").modal();
    	   var name = $("#container7 .navbar-brand").text();
    	   $("#myModal2 label").text(name);
       });
       
       $(".glyphicon-remove").click(function(){
   	    $("#container7").hide();
   	    $("#container8").hide();
   	    $("#container9").hide();
       });
       
  
       
  });