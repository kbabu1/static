
var apiKey = ""; // Replace with your API key. See https://dashboard.tokbox.com/projects
var sessionId = ""; // Replace with your own session ID. See https://dashboard.tokbox.com/projects
var token = "" ;  // Replace with a generated token. See https://dashboard.tokbox.com/projects

var username="";
var session;
var publisher;

var VIDEO_WIDTH = 361;
var VIDEO_HEIGHT = 347;


var _selfstream;
var  subscribers = new Array();
var _streams = new Array();
var _connections = new Array();

function connect(apiKey1,sessionId1,token1,username1) {
    console.log("Connect "+token1);
    OT.on("exception", exceptionHandler);
    apiKey=apiKey1;
    sessionId=sessionId1;
	username=username1;
		console.log("============= USERNAME =========== "+username);

    token=token1;
    // Un-comment the following to set automatic logging:
    OT.setLogLevel(OT.DEBUG);

    if (!(OT.checkSystemRequirements())) {
        alert("You don't have the minimum requirements to run this application.");
    } else {
        session = OT.initSession(sessionId);	// Initialize session
        // Connect to the session
        session.connect(apiKey,token);

        // Add event listeners to the session

        session.on('sessionConnected', sessionConnectedHandler);
        session.on('sessionDisconnected', sessionDisconnectedHandler);
        session.on('connectionCreated', connectionCreatedHandler);
        session.on('connectionDestroyed', connectionDestroyedHandler);
        session.on('streamCreated', streamCreatedHandler);
        session.on('streamDestroyed', streamDestroyedHandler);
        session.on("signal", signalEventHandler);


        setupChat();


    }



}
function setupChat(){
    hide("chatContainer");
    _userName="user";
    //  _userName = document.getElementById("txtName").value;

    /* var chatButton = document.createElement("button");
     chatButton.innerHTML = "Post question";
     chatButton.setAttribute("id", _chatID);
     chatButton.setAttribute("onClick", "postQuestion(this)");
     chatButton.setAttribute("class", "chatButton btn btn-primary btn-sm");

     var chatBox = document.createElement("textarea");

     chatBox.setAttribute("placeholder", "Type in a message");
     chatBox.setAttribute("id", "chatbox");
     chatBox.setAttribute("class", "chatBox form-control");
     chatBox.setAttribute("style", "width: 272px; height: 54px");
     chatBox.addEventListener("keypress", function(e){
     if (e.keyCode == 13){
     postQuestion(chatBox);
     }
     });


     var chatHistory = document.createElement("div");
     chatHistory.setAttribute("id", "chathistory");
     chatHistory.setAttribute("class", "chatHistory");
     chatHistory.innerHTML = "<strong><h4><b>Welcome " + _userName + "!</b></h4></strong>";

     document.getElementById("chatContainer").appendChild(chatHistory);  //Add the above created elements to the chatContainer element in the html pages
     document.getElementById("chatContainer").appendChild(chatBox);
     document.getElementById("chatContainer").appendChild(chatButton);*/



    var chatBox = document.getElementById("msgTxt");
    chatBox.addEventListener("keypress", function(event){
        if (event.keyCode == 13){
            event.preventDefault();
			        //    var userList = document.getElementById("contactList");
					   var userList=	document.getElementsByClassName('chat-user');

            var targetUser = '';
			console.log("USER LIST ",userList);

            for (var i = 0; i < userList.length; i++) {
										console.log(userList[i].value);
targetUser=userList[i].value;
               /* if (userList.children[i].nodeName === "BUTTON") {
                    targetUser = userList.children[i].getAttribute("id");
                    targetUser = (targetUser.replace("btn_", ""));

				}*/
				
                    var streamTarget = _streams[targetUser];
											console.log("USER LIST _streams ",streamTarget);

					}

            session.signal({
                               type: 'msg',
                               data: msgTxt.value
                           }, function(error) {
                if (error) {
                    console.log('Error sending signal:', error.name, error.message);
                } else {
                    msgTxt.value = '';
                }
            });        }
    });
    /*$("#msgInput input").keypress(function(e){
     if(e.which == 13) {
     $("#send").click();
     }
     });*/

}
function postMessage(){}

function disconnect() {
    //stopPublishing();
    session.disconnect() ;

  //  hide('disconnectLink');

}


function sessionConnectedHandler(event) {
    console.log(" sessionConnectedHandler ",event);
}

function streamCreatedHandler(event) {
            addButton(event.stream);

}

function addButton( selectedStream) {
    console.log(" addButton ",selectedStream);
    console.log(" addButton ",selectedStream.streamId);



}
function removeButton(selectedStream) {
    console.log(" removeButton ",selectedStream);
    console.log(" removeButton ",selectedStream.streamId);

}


function endCall(obj, label) {
    console.log(" endCall ",obj);
    console.log(" endCall ",label);
    console.log("endcall called");
    console.log(obj.value);

}

function beginCall(obj) {
    console.log(" beginCall ",obj);

    console.log(obj.value);






}


function signalEventHandler(event) {

    console.log(" signalEventHandler ",event);

    if (event.type == "signal:msg") {

  var msg = document.createElement('p');
  msg.innerText = username+" : "+event.data;
  msg.className = event.from.connectionId === session.connection.connectionId ? 'mine' : 'theirs';
  var msgHistory = document.querySelector('#history');
	  console.log("msgHistory",msgHistory);

  msgHistory.appendChild(msg);
       //document.getElementById("history").innerHTML += msg.innerText;


  msg.scrollIntoView();
	}
}

function streamDestroyedHandler(event) {
    // This signals that a stream was destroyed. Any Subscribers will automatically be removed.
    // This default behaviour can be prevented using event.preventDefault()
    removeButton(event.stream);
}

function sessionDisconnectedHandler(event) {
    // This signals that the user was disconnected from the Session. Any subscribers and publishers
    // will automatically be removed. This default behaviour can be prevented using event.preventDefault()

    session.off('sessionConnected', sessionConnectedHandler);
    session.off('streamCreated', streamCreatedHandler);
    session.off('streamDestroyed', streamDestroyedHandler);
    session.off('connectionCreated', connectionCreatedHandler);
    session.off("signal", signalEventHandler);
    OT.off("exception", exceptionHandler);
    session.off('sessionDisconnected', sessionDisconnectedHandler);
    publisher = null;
    removeAllButtons();
    show('connectLink');
    hide('disconnectLink');

}



function connectionDestroyedHandler(event) {
    // This signals that connections were destroyed
}

function connectionCreatedHandler(event) {
    // This signals new connections have been created.
}

/*
 If you un-comment the call to OT.setLogLevel(), above, OpenTok automatically displays exception event messages.
 */
function exceptionHandler(event) {
    alert("Exception: " + event.code + "::" + event.message);
}

//--------------------------------------
//  HELPER METHODS
//--------------------------------------

function addStream(stream) {
    // Check if this is the stream that I am publishing, and if so do not publish.
    if (stream.connection.connectionId == session.connection.connectionId) {
        return;
    }
    var subscriberDiv = document.createElement('div'); // Create a div for the subscriber to replace
    subscriberDiv.setAttribute('id', stream.streamId); // Give the replacement div the id of the stream as its id.
    document.getElementById("subscribers").appendChild(subscriberDiv);
    var subscriberProps = {width: VIDEO_WIDTH, height: VIDEO_HEIGHT};
    subscribers[stream.streamId] = session.subscribe(stream, subscriberDiv.id, subscriberProps);
}


function removeStream(stream)
{
    session.unsubscribe(subscribers[stream.streamId]);
}
function show(id) {
    //document.getElementById(id).style.display = 'block';
}

function hide(id) {
    // document.getElementById(id).style.display = 'none';
}
        