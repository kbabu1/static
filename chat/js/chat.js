  $(document).ready(function(){

	  $("#container1 nav ul").hide();
	  $(".chatBox").hide();
	  $("#container1 nav").click(function(){
             $("#container1 nav ul").show();
	     $(".chatBox").show();
        $("#container1").css("height", "495");
	     
	 });
	
    $("#container2").hide();
    $(".fa-ellipsis-h").click(function(){
    	$("#container2").show();
     
    });

    $(".fa-close").click(function(){
      $("#container2").hide();
      $("#container3").hide();
      $("#container5").hide();
      $("#container6").hide();
    });

    $("#container3").hide();
    $(".fa-bell").click(function(){
      $("#container3").show();
      $("#container4").hide();
    });
    
    $("#container4").hide();
    
	 $(".fa-edit").unbind('click').click(function(){
	        $("#container4").toggle();
	     });

  
    $('input[type=radio]').change(function () {
      $('[type=radio]:checked').prop('checked', false);
      $(this).prop('checked', true);
     });

    $("#container5").hide();
    $("#broadcast").click(function(){
      $("#container5").show();
      $("#container4").hide();
    });

    $("#container6").hide();
    $("#newGroup").click(function(){
      $("#container6").show();
      $("#container4").hide();
    });
    
    $("#createGroup").unbind('click').click(function(){
        var grpname = $("#groupList input[type='text'] ").val();
        var password = $("#groupList input[type='password']").val();
        var pswrd = $("#groupList li:eq(1)").hasClass("pswrd");
         if(pswrd == true)
                    {
                        if(grpname != "" && password != ""){
                        	alert("New Group Created");
                        }
                        else if(grpname != "" && password == ""){
                        	alert("Please enter password");
                        }
                        else{
                        	alert("Group name can't be blank");
                        }
                    }
                    else{
                    	if(grpname != ""){
                        	alert("New Group Created");
                        }
                    	else{
                        	alert("Group name can't be blank");
                        }
                    }
                
      });
    
    $("#select").unbind('change').change(function(){
    	var option= $("#select")[0].selectedIndex;
    	var password ="<li class='list-group-item pswrd'><input type='password' placeholder='Password' required></li>";
        if(option == 1){
            $("#groupList li:eq(0)").after(password);
        }
        else{
         	var pswrd = $("#groupList li:eq(1)").hasClass("pswrd");
            if(pswrd == true)
            {
              $("#groupList li:eq(1)").remove(".pswrd");
            }
        }
    });
      	
    /* Search */
    $("#myInput").keyup(function(){
       var input, filter;
       input = $(this).val().toLowerCase();
       $(".left-side li").hide();
       $("li").each(function(index) {
    	 filter= $("div>div>h4", this).text().toLowerCase() ;
    	  if(filter.indexOf(input) == 0){
    	    $(this).show();
          }
        })
  	 });
/* $("#msgInput input").keypress(function(e){
        if(e.which == 13) {
        	var chat = $("#msgInput input").val();
            if(chat != ""){
             	$("#chatArea").append("<p>" + chat + "</p>");
            	$("<span class='glyphicon glyphicon-triangle-top'></span><br>").insertBefore("#chatArea  p:last");
           	    $("#chatArea p").css({"background-color" : "#6DA8E3",  "float" : "right", "clear" : "both", "min-width" : "50px", "margin" : "0px 10px 0px auto", "border" : "1px solid #6DA8E3", "border-radius" : "10px"});
                $("#msgInput input").val("");
          }
        }     
    });*/
    
    $("#recent").hide();
    $("#groups").hide();
    $(".chatBox ul li:eq(0)").click(function(){
        $("#contact").hide();
        $("#groups").hide();
        $("#recent").show();
        $(".chatBox ul li:eq(1), .chatBox ul li:eq(2)").removeClass("active");
        $(".chatBox ul li:eq(0)").addClass("active");
      });
    
    $(".chatBox ul li:eq(1)").click(function(){
        $("#contact").show();
        $("#groups").hide();
        $("#recent").hide();
        $(".chatBox ul li:eq(0), .chatBox ul li:eq(2)").removeClass("active");
        $(".chatBox ul li:eq(1)").addClass("active");
      });
    
    $(".chatBox ul li:eq(2)").click(function(){
        $("#contact").hide();
        $("#recent").hide();
        $("#groups").show();
        $(".chatBox ul li:eq(0), .chatBox ul li:eq(1)").removeClass("active");
        $(".chatBox ul li:eq(2)").addClass("active");
      });
  
    

          /*Count of Group Members */
          $("#grp").click(function(){
           $("#groups .left-side li").each(function() {
           var grpName = $("div>div>h4", this).text();
            console.log(grpName);
            var ul_count = $(".groupMembers ul").length;
            var grpClass = $(".groupMembers ul").hasClass(grpName);
            if(grpClass == true){
               var count = $(".groupMembers ."+ grpName).children().length;
              }
              $("div>div>p", this).text("Participants: "+count);
        });
        });
});